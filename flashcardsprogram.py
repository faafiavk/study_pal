# Importing info from another file, importing from files in the library, Example of importing stuff from other pages
# import flashcard
# import random
# flashcard.createNewFlashCard()

# Sets List
print('Welcome to Physics Flashcards - NCEA Level 2')
sets_list = ['mechanics', 'waves', 'light']

print(sets_list)

set = input('What set? ')
# Mechanics Terms List
if set == 'mechanics':
    print('Mechanics Flashcards')
    Mechanics_terms = ['force', 'velocity', 'momentum']

# Mechanics Terms Definition
    Mechanics_def = ['force = mass times acceleration',
                     'velocity = distance over time',
                     'momentum(p) = mass times velocity']

# List of mechanic terms to assist the user in picking terms
    print(Mechanics_terms)

# Flashcard #1 - Definitions based on what term the user types
    print('FLASHCARD #1')
    fc = input('Term: ')
    if fc == 'force':
        print('definition:', Mechanics_def[0])
    elif fc == 'velocity':
        print('definition: ', Mechanics_def[1])
    elif fc == 'momentum':
        print('definition: ', Mechanics_def[2])

# Flashcard #2 - "        "
    print('FLASHCARD #2')
    fc = input('Term: ')
    if fc == 'force':
        print('definition:', Mechanics_def[0])
    elif fc == 'velocity':
        print('definition: ', Mechanics_def[1])
    elif fc == 'momentum':
        print('definition: ', Mechanics_def[2])

# Flashcard #3 - "            "
    print('FLASHCARD #3')
    fc = input('Term: ')
    if fc == 'force':
        print('definition:', Mechanics_def[0])
    elif fc == 'velocity':
        print('definition: ', Mechanics_def[1])
    elif fc == 'momentum':
        print('definition: ', Mechanics_def[2])

# Waves Terms List
if set == 'waves':
    print('Waves Flashcards')
    Waves_terms = ['real', 'inverted', 'diminished', 'enlarged']

# Waves Terms Definition
    Waves_def = ['Image is formed by real light rays',
                 'Image is upside down when compared to the object',
                 'Image is smaller than the object',
                 'Image is larger than the object']
    print(Waves_terms)

# Waves Flashcards

    print('FLASHCARD #1')
    fc = input('Term: ')

    if fc == 'real':
        print('definition:', Waves_def[0])
        print('\n')
    elif fc == 'inverted':
        print('definition: ', Waves_def[1])
        print('\n')
    elif fc == 'diminished':
        print('definition: ', Waves_def[2])
        print('\n')
    elif fc == 'enlarged':
        print('definition: ', Waves_def[3])
        print('\n')

    print('FLASHCARD #2')
    fc = input('Term: ')
    if fc == 'real':
        print('definition:', Waves_def[0])
        print('\n')
    elif fc == 'inverted':
        print('definition: ', Waves_def[1])
        print('\n')
    elif fc == 'diminished':
        print('definition: ', Waves_def[2])
        print('\n')
    elif fc == 'enlarged':
        print('definition: ', Waves_def[3])
        print('\n')

    print('FLASHCARD #3')
    fc = input('Term: ')
    if fc == 'real':
        print('definition:', Waves_def[0])
        print('\n')
    elif fc == 'inverted':
        print('definition: ', Waves_def[1])
        print('\n')
    elif fc == 'diminished':
        print('definition: ', Waves_def[2])
        print('\n')
    elif fc == 'enlarged':
        print('definition: ', Waves_def[3])
        print('\n')

    print('FLASHCARD #4')
    fc = input('Term: ')
    if fc == 'real':
        print('definition:', Waves_def[0])
    elif fc == 'inverted':
        print('definition: ', Waves_def[1])
    elif fc == 'diminished':
        print('definition: ', Waves_def[2])
    elif fc == 'enlarged':
        print('definition: ', Waves_def[3])

# Light Terms List
if set == 'light':
    print('Light Flashcards')
    Light_terms = ['Concave', 'Convex', 'Focal Length']

# Light Terms Definition
    Light_def = ['Like a cave the reflective surface curves inwards.',
                 'Reflective surface bumps out towards the light source.',
                 'Distance between the focus and the centre of the mirror']
    print(Light_terms)

    # Flashcard practice
    print('FLASHCARD #1')
    fc = input('Term: ')

    if fc == 'concave':
        print('definition:', Light_def[0])
    elif fc == 'convex':
        print('definition: ', Light_def[1])
    elif fc == 'focal length':
        print('definition: ', Light_def[2])

    print('FLASHCARD #2')
    fc = input('Term: ')
    if fc == 'concave':
        print('definition:', Light_def[0])
    elif fc == 'convex':
        print('definition: ', Light_def[1])
    elif fc == 'focal length':
        print('definition: ', Light_def[2])

# Flashcard #3 - Definitions based on what term the user types
    print('FLASHCARD #3')
    fc = input('Term: ')
    if fc == 'concave':
        print('definition:', Light_def[0])
    elif fc == 'convex':
        print('definition: ', Light_def[1])
    elif fc == 'focal length':
        print('definition: ', Light_def[2])


print('Congrats! Studied all flashcards :)')

print('Return to menu?')