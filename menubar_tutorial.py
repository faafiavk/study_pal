import tkinter as tk
from tkinter import ttk
from tkinter import *
from random import randint
import csv
import random

# Global variables
words = []
line_count = 0

# opening and setting up the window
root = tk.Tk()
root.title("Physics Flashcards - Mechanics Level 2")
root.geometry("550x410")


# Opens the help window through the menu bar
def open_new_window():
    # Top level object which will be treated as a new window
    new_window = Toplevel(root)
    new_window.title("Help")
    # Opens next to the main window as a smaller window
    new_window.geometry("600x300+700+100")

    # The information on the help window is taken from the readme.txt file
    read_me = []
    with open("readme.txt", "r") as text_file:
        for row in text_file:
            read_me.append(row)

    readme = Label(new_window, text=read_me)
    readme.place(x=50, y=50)


def set_sub_mechanics():
    global words, line_count
    with open('mechanics.txt', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            words.append(row)
            if row != "\n":
                line_count += 1


def set_sub_waves():
    global words, line_count
    with open('waves.txt', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            words.append(row)
            if row != "\n":
                line_count += 1

def set_sub_light():
    global words, line_count
    with open('light.txt', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            words.append(row)
            if row != "\n":
                line_count += 1


# menu bar
menu_bar = tk.Menu(root)

# Menu
menu = tk.Menu(menu_bar, tearoff=False)
menu.add_command(label="New set")
menu.add_command(label="Help", command=open_new_window)

menu_bar.add_cascade(label="Menu", menu=menu)

# the different sets on the menu bar
mechanics = tk.Menu(menu_bar, tearoff=False)
menu_bar.add_command(label="Select Mechanics", command=set_sub_mechanics)
# menu_bar.add_cascade(label="Mechanics", menu=mechanics, command=mechanics)

waves = tk.Menu(menu_bar, tearoff=False)
menu_bar.add_command(label="Select Waves", command=set_sub_waves)
# menu_bar.add_cascade(label="Waves", menu=waves)

light = tk.Menu(menu_bar, tearoff=False)
menu_bar.add_command(label="Select Light", command=set_sub_light)
# menu_bar.add_cascade(label="Light", menu=light)

root.config(menu=menu_bar)

# Retrieves the words(terms) from a text file




# defines the function of the next button
def next():
    global hinter, hint_count
    # Clear the screen
    answer_label.config(text="")
    my_entry.delete(0, END)
    hint_label.config(text="")
    # reset hint stuff
    hinter = ""
    hint_count = 0

    # select random term
    global random_word
    random_word = randint(0, line_count)
    # update label with term
    term.config(text=words[random_word][0])



# checks if the user's input is the correct answer
def answer():
    if my_entry.get() == words[random_word][1].strip():
        answer_label.config(text=f"Correct! {words[random_word][0]} is {words[random_word][1].strip()}")
    else:
        answer_label.config(text=f"Incorrect! {words[random_word][0]} is {words[random_word][1].strip()}")


# Keep track of hints
hinter = ""
hint_count = 0


# Each time the hint button is pressed the user gets one letter that contributes/reveals the correct answer
def hint():
    global hint_count
    global hinter
    if hint_count < len(words[random_word][1]):
        hinter = hinter + words[random_word][1][hint_count]
        hint_label.config(text=hinter)
        hint_count += 1


# Term Label
term = Label(root, text="", font=("Helvetica", 18))
term.pack(pady=50)

# Answer label (correct or incorrect)
answer_label = Label(root, text="")
answer_label.pack(pady=20)


# User input text box
my_entry = Entry(root, font=("Helvetica", 14))
my_entry.pack(pady=20)

# Create buttons
button_frame = Frame(root)
button_frame.pack(pady=20)

# Answer button
answer_button = Button(button_frame, text="Answer", command=answer)
answer_button.grid(row=0, column=0, padx=20)

# Next button
next_button = Button(button_frame, text="New Term", command=next)
next_button.grid(row=0, column=1,)

# Hint button
hint_button = Button(button_frame, text="Hint", command=hint)
hint_button.grid(row=0, column=2, padx=20)

# Create hint label
hint_label = Label(root, text="")
hint_label.pack(pady=20)

# Run next function when program starts
# next()
root.mainloop()
