import tkinter as tk
from tkinter import ttk
from tkinter import *
from random import randint
import csv
import random

# Global variable
words = []


# opening and setting up the window
root = tk.Tk()
root.title('Physics Flashcards - Mechanics Level 2')
root.geometry("550x410")


# Opens the help window in the menu
def open_new_window():
    # Top level object which will be treated as a new window
    new_window = Toplevel(root)
    new_window.title("Help")
    # Opens next to the main window as a smaller window
    new_window.geometry("300x300+700+100")

def button_press():
    global words
    global mechanics, waves, light

    if mechanics:
        with open('mechanics.txt', 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                words.append(row)
                random.shuffle(row)
                print(row)

    elif waves:
        with open('waves.txt', 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                words.append(row)
                random.shuffle(row)

    elif light:
        with open('light.txt', 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                words.append(row)
                random.shuffle(row[0][1])

# menu bar
menubar = tk.Menu(root)

# Menu
menu = tk.Menu(menubar, tearoff=False)
menu.add_command(label="New set")
menu.add_command(label="Help", command=open_new_window)

menubar.add_cascade(label="Menu", menu=menu)

# the different sets on the menu bar
mechanics = tk.Menu(menubar, tearoff=False)
menubar.add_cascade(label="Mechanics", menu=mechanics, command=button_press)

waves = tk.Menu(menubar, tearoff=False)
menubar.add_cascade(label="Waves", menu=waves, command=button_press)

light = tk.Menu(menubar, tearoff=False)
menubar.add_cascade(label="Light", menu=light, command=button_press)

root.config(menu=menubar)

# Retrieves the words(terms) from a text file



# defines the function of the next button
def next():
    global hinter, hint_count
    # Clear the screen
    answer_label.config(text="")
    my_entry.delete(0, END)
    hint_label.config(text="")
    # reset hint stuff
    hinter = ""
    hint_count = 0

    # select random term
    term.config(text=words[0])


# function of the answer button
def answer():
    if my_entry.get() == words[1].strip():
        answer_label.config(text=f"Correct! {words[0]} is {words[1].strip()}")
    else:
        answer_label.config(text=f"Incorrect! {words[0]} is {words[1].strip()}")


# Keep track of hints
hinter = ""
hint_count = 0
def hint():
    global hint_count
    global hinter

    if hint_count < len(words[1]):
        hinter = hinter + words[1][hint_count]
        hint_label.config(text=hinter)
        hint_count += 1


# Term Label
term = Label(root, text="", font=("Helvetica", 18))
term.pack(pady=50)

# answer label (correct/incorrect and prints out the definition
answer_label = Label(root, text="")
answer_label.pack(pady=20)

# User input text box
my_entry = Entry(root, font=("Helvetica", 14))
my_entry.pack(pady=20)

# create buttons
button_frame = Frame(root)
button_frame.pack(pady=20)

# answer button
answer_button = Button(button_frame, text="Answer", command=answer)
answer_button.grid(row=0, column=0, padx=20)

# next button
next_button = Button(button_frame, text="Next", command=next)
next_button.grid(row=0, column=1,)

# hint button
hint_button = Button(button_frame, text="Hint", command=hint)
hint_button.grid(row=0, column=2, padx=20)

# Create hint label
hint_label = Label(root, text="")
hint_label.pack(pady=20)

# Run next function when program starts
next()

root.mainloop()
