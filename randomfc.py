import random

# def createNewFlashCard(): defining variables practice
    # print("adkljaldfkj")

# Picks a random line from a text file
def random_line(filename):
    lines = open(filename).read().splitlines()
    return random.choice(lines)
print(random_line('test2.txt'))

# random position
with open("test2.txt", "r") as file:
    data = file.read()
    words = data.split()
    word_pos = random.randint(0, len(words) - 1)
    print("Position:", word_pos)
    print("Word at position:", words[word_pos])