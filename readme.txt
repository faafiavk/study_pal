1) Select set from the menu
2) Click the new term button to pick a random term from the set
3) Enter your definition into the text box
3a) Click the hint button twice to reveal the first letter of the definition if help is needed
3b) Continue clicking the hint button to reveal more letters of the definition if needed.
4) Press answer button to check if your answer is correct
5) Underneath the term it will say if the answer was correct or not and display the correct definition.
6) Click new term to pick another random term from the set

Note: To find out what sets you've selected there will be a tick next to the chosen set(s)