import tkinter as tk
from tkinter import *
from random import randint
import csv

# Global variables
# Words is the list of all terms and definitions that from the text file(s)
words = []

# line count is used to count the lines in the text file and randomise the terms
line_count = 0

# background colour for the root window
bg = "light blue"

# Window settings
root = tk.Tk()
root.title("Study Flash ⚡")
root.configure(background=bg)
root.geometry("550x410")

# Study Flash Title
title = Label(root, text="Study Flash ⚡", font=("Arial", 20))
title.place(x=190, y=10)


# Opens the help window through the menu bar
def open_help_window():
    # Top level object which will be treated as a new window
    new_window = Toplevel(root)
    new_window.title("Help")
    # Opens next to the main window as a smaller window
    new_window.configure(background="pink")
    new_window.geometry("600x300+700+100")

    # The information for the help window is taken from the readme text file
    file = open("readme.txt")
    read_me = file.read()
    readme = Label(new_window, text=read_me)
    instructions = Label(new_window, text="Instructions for Study Flash ⚡")
    readme.place(x=50, y=50)
    instructions.place(x=30, y=30)


# The text file used depends on the menu button the user presses (mechanics, waves or light)
def set_sub_mechanics():
    global words, line_count
    with open('mechanics.txt', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            words.append(row)
            if row != "\n":
                line_count += 1


def set_sub_waves():
    global words, line_count
    with open('waves.txt', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            words.append(row)
            if row != "\n":
                line_count += 1


def set_sub_light():
    global words, line_count
    with open('light.txt', 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            words.append(row)
            if row != "\n":
                line_count += 1


# menu bar
menu_bar = tk.Menu(root)

# How to play
how_to_play = tk.Menu(menu_bar, tearoff=False)
menu_bar.add_checkbutton(label="How To Play", command=open_help_window)

# Physics Sets
physics_sets = tk.Menu(menu_bar, tearoff=False)
physics_sets.add_checkbutton(label="Select Mechanics", command=set_sub_mechanics)
physics_sets.add_checkbutton(label="Select Waves", command=set_sub_waves)
physics_sets.add_checkbutton(label="Select Light", command=set_sub_light)
menu_bar.add_cascade(label="Physics Level 2 Sets", menu=physics_sets)

root.config(menu=menu_bar)


# New term button clears all the labels and buttons and gives the user another term
def new_term():
    global hinter, hint_count
    # Clear the screen
    answer_label.config(text="")
    my_entry.delete(0, END)
    hint_label.config(text="")
    # reset hint stuff
    hinter = ""
    hint_count = 0

    # select random term
    global random_word
    random_word = randint(0, line_count-1)
    # update label with term
    term.config(text=words[random_word][0])


# checks if the user's input matches(correct/incorrect) the definition in the text file
def answer():
    if my_entry.get() == words[random_word][1].strip():
        answer_label.config(text=f"Correct! {words[random_word][0]} is {words[random_word][1].strip()}")
    else:
        answer_label.config(text=f"Incorrect! {words[random_word][0]} is {words[random_word][1].strip()}")


# Keep track of hints
hinter = ""
hint_count = 0


# Reveals a letter from the definition each time the button is pressed
def hint():
    global hint_count
    global hinter
    if hint_count < len(words[random_word][1]):
        hinter = hinter + words[random_word][1][hint_count]
        hint_label.config(text=hinter)
        hint_count += 1


# Term Label
term = Label(root, text="", background=bg, font=("Helvetica", 18))
term.pack(pady=70)

# Answer label (correct or incorrect)
answer_label = Label(root, background=bg, text="")
answer_label.pack(pady=10)


# User input text box
my_entry = Entry(root, font=("Helvetica", 14))
my_entry.pack(pady=20)

# Create buttons
button_frame = Frame(root, background=bg)
button_frame.pack(pady=20)

# Answer button
answer_button = Button(button_frame, text="Answer", command=answer, activebackground="pink", activeforeground="blue")
answer_button.grid(row=0, column=0, padx=20)

# Next button
new_term_button = Button(button_frame, text="New Term", command=new_term, activebackground="pink", activeforeground="blue")
new_term_button.grid(row=0, column=1,)

# Hint button
hint_button = Button(button_frame, text="Hint", command=hint, activebackground="pink", activeforeground="blue")
hint_button.grid(row=0, column=2, padx=20)

# Create hint label
hint_label = Label(root, background=bg, text="")
hint_label.pack(pady=20)

root.mainloop()
